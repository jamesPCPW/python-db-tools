#!/usr/bin/env python

import MySQLdb
import csv
import time
import sys


# Notes:
#  - Exports a CSV file from SQL Query
#  - Assumes Headers will be passed in export_query.
#  - Example usage, python export_csv.py root password db sql/export_query.sql host
#
def main(user, passwd, db, sql, filename, host="localhost"):
    try:
        conn = get_conn(user=user, passwd=passwd, db=db, host=host)
    except MySQLdb.Error, e:
        print "Error %d: %s" % (e.args[0], e.args[1])
        sys.exit(1)

    cursor = conn.cursor()

    sql_file = open(sql, 'r')
    stmt = sql_file.read()

    cursor.execute(stmt)

    # Append a date to end the of the file, to indicate the date it was exported.
    with open(filename + "_" + time.strftime("%Y-%m-%d") + '.csv', 'wb') as f:
        writer = csv.writer(f, delimiter=',',
                            lineterminator='\r\n',
                            quoting=csv.QUOTE_NONNUMERIC)

        # write the headers to the file.
        field_names = [i[0] for i in cursor.description]
        writer.writerow(field_names)

        for row in cursor.fetchall():
            writer.writerow(row)

    conn.close()


def get_conn(user, db, passwd, host):
    conn = MySQLdb.connect(host=host,
                           user=user,
                           passwd=passwd,
                           db=db)
    return conn


if __name__ == '__main__':
    # command line execution..
    args = sys.argv[1:]

    if len(args) < 5:
        print 'Error; User, password, db and sql and export file name is required'
        sys.exit(1)

    main(*args)
